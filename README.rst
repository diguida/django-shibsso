==============================================================
ShibSSO - Shibboleth |R| Authentication Backend for Django |R|
==============================================================

With ShibSSO you can integrate your Django project with Shibboleth.
Because ShibSSO was created for the Django authentication system, you can use
all features that come with Django for authorization purposes.
Moreover, you can change the authentication backend anytime without
compatibility issues.

First, you need to configure Apache |R| and Shibboleth.
Then configure your project and set the permissions you want for each view.

ShibSSO was tested for Django 1.1.1, 1.2 and 1.2.1.

::::::::::::::::::::
Apache Configuration
::::::::::::::::::::

On your Apache configuration, where you have::

    SSLRequireSSL   # The modules only work using HTTPS
    AuthType shibboleth
    ShibRequireSession On
    ShibRequireAll On
    ShibExportAssertion Off

    Require valid-user
    Require adfs-group "Some Users Group", "Some Other Users Group"

change to::

    AuthType shibboleth
    Require shibboleth

With these changes the default behavior is to accept any user.
Shibboleth is only used to authenticate the user (if necessary) and pass the
information to your Django application.
All authorization procedures are handled by the Authentication Backend.

You can still use the upper configuration if you want all users to be logged
in in any page of your website.

::::::::::::::::::::::::::::
Django Project Configuration
::::::::::::::::::::::::::::

Installation
++++++++++++

::

    python setup.py install

Configuration
+++++++++++++

url.py
......

First, you need to configure where the login and logout views will be mapped on
your application.
Because these views are SSO pages, you can not change their appearances.
You can only define the local path to them.

Add `shibsso.views.login` and `shibsso.views.logout` views to `urlpatterns`.

Example::

    urlpatterns = patterns('',
        ...
        (r'^accounts/login/$', 'shibsso.views.login'),
        (r'^accounts/logout/$', 'shibsso.views.logout'),
        ...
        )

settings.py
...........

If you are not using `'/accounts/login/'` for the login view, you will need to
update `LOGIN_URL`.
See `User authentication in Django`_ for more information.

.. _User authentication in Django: http://docs.djangoproject.com/en/dev/topics/auth/

Add:

* `shibsso.middleware.ShibSSOMiddleware` to `MIDDLEWARE_CLASSES`.

  Make sure that `shibsso.middleware.ShibSSOMiddleware` is installed after
  `django.contrib.auth.middleware.AuthenticationMiddleware`.

  Example:

   ::

    MIDDLEWARE_CLASSES = (
        'django.middleware.common.CommonMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'shibsso.middleware.ShibSSOMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
    )

* `shibsso.backends.ShibSSOBackend` to `AUTHENTICATION_BACKENDS`.

  Example:

   ::

    AUTHENTICATION_BACKENDS = (
        'shibsso.backends.ShibSSOBackend',
    )

* `shibsso` to `INSTALLED_APPS` (if you are using the administration interface,
  optional otherwise).

  You must insert `shibsso` right after `django.contrib.admin` and before all
  the other application.

  Example:

   ::

    INSTALLED_APPS = (
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.admin',
        'shibsso',
        'myapp',
    )

Then you need to configure ShibSSO:

* `SHIB_SSO_LOGIN_PATH`

  Local path to your SSO login page with the query string to the next page at
  the end;

* `SHIB_SSO_LOGOUT_URL`

  Logout url for SSO with the query string to the next page at the end;
   
* `SHIB_SSO_ADMIN = True|False`

  If the administration interface is going to authenticate users via Shibboleth;

* `SHIB_SSO_CREATE_ACTIVE = True|False`

  If the user does not exist in the database, create it as active;

* `SHIB_SSO_CREATE_STAFF = True|False`

  If the user does not exist in the database, create it as staff;

* `SHIB_SSO_CREATE_SUPERUSER = True|False`

  If the user does not exist in the database, create it as superuser;

Shibboleth mapping rules must also be configured:

* `META_EMAIL`

  Email of the user;

* `META_FIRSTNAME`

  First name of the user;

* `META_GROUP`

  List of groups the user belongs to (comma separated);

* `META_LASTNAME`

  Last name of the user;

* `META_USERNAME`

  Login of the user.

Shibboleth mapping rules can be found in (usually located under `/etc/shibboleth/`):

* `AAP.xml` for Shibboleth 1.3;

* `attribute-map.xml` for Shibboleth 2.x.

Example values are::

    SHIB_SSO_ADMIN = True
    SHIB_SSO_CREATE_ACTIVE = True
    SHIB_SSO_CREATE_STAFF = False
    SHIB_SSO_CREATE_SUPERUSER = False
    SHIB_LOGIN_PATH = '/Shibboleth.sso/?target='
    SHIB_LOGOUT_URL = 'https://logout.myserver.ch/returnurl='
    META_EMAIL = 'SHIB_EMAIL'
    META_FIRSTNAME = 'SHIB_FIRSTNAME'
    META_GROUP = 'SHIB_GROUP'
    META_LASTNAME = 'SHIB_LASTNAME'
    META_USERNAME = 'SHIB_LOGIN'

:::::::::::::::::::::::::::::
How to login and logout users
:::::::::::::::::::::::::::::

Login
+++++

Once you have configure your application, it is very easy to authenticate a user.
To do so, just create a link in your page to the `shibsso.views.login` view.
For the configuration presented before, just create a link to `/accounts/login/`.

Example::

    <a href="/accounts/login/"> Login </a>

After the login, the user will be automatically redirected to
`settings.LOGIN_REDIRECT_URL` (which defaults to `/accounts/profile/`).
If you want a different page to be shown use::

    <a href="/accounts/login/?next=/myprofile"> Login </a>

Logout
++++++

Create a link in your page to the path that maps the `shibsso.views.logout` view.
For the example presented before, just create a link to `/accounts/logout/` on
your webpage::

    <a href="/accounts/logout/"> Logout </a>

Once logged out, the default logout template will be shown.

The logout view can be configured using optional arguments:

* `next_page`

  The URL to redirect to after logout;

* `template_name`

  The full name of a template to display after logging the user out.
  This will default to =registration/logged_out.html= if no argument is supplied.

* `redirect_field_name`

  The name of a GET field containing the URL to redirect to after log out (which
  defaults to =next=).
  Overrides `next_page` if the given GET parameter is passed.

If you want to redirect to a specific page, use the `redirect_field_name` in a
query string.
Example::

    <a href="/accounts/logout/?next=/mygoodbye"> Logout </a>

::::::::::::::::::::::::::::::::::::::::::
Getting information about the current user
::::::::::::::::::::::::::::::::::::::::::

Within a view, information about the user can easily be obtained.
To do so, you just need to get the `User` object from the request::

    session_user = request.user

`User` objects have the following attributes:

* `username` - Username;

* `first_name` - First name of the user;

* `last_name` - Last name of the user;

* `email` - Email of the user;

* `groups` - Many-to-many field to access the groups of the user;

* `is_staff` - Designates whether this user can access the admin site;

* `is_active` - Designates whether this user account should be considered active;

* `is_superuser` - Designates that this user has all permissions without
  explicitly assigning them.

Please note that `User` is a native Django class.
For more information about `User` see the `User API Reference`_.

.. _User API Reference: http://docs.djangoproject.com/en/dev/topics/auth/#api-reference

:::::::::::::::::::::::::::::::::
How users are created and updated
:::::::::::::::::::::::::::::::::

Each time a new user logs in, a new entry in the database is created according
to `settings.SHIB_SSO_CREATE_ACTIVE`, `settings.SHIB_SSO_CREATE_STAFF` and
`settings.SHIB_SSO_CREATE_SUPERUSER`.

Each time a user logs in the following fields are updated:

* `first_name`;

* `last_name`;

* `email`;

* `password` to `'(not used)'`.
  The real password is never stored in the database!

Regarding the groups, only groups that exist on the database are used.
For example, if you want to use the group `'my-group'` for authorization
purposes, you need to add the group `'my-group'` to the `auth_group` table
(you can use the administration interface for that).
You can also use Django permissions for each group.

The fields are only updated when the user logs in.
For existing users, `is_staff`, `is_active` and `is_superuser` are never updated.

:::::::::::::::::::::::::
How to create a superuser
:::::::::::::::::::::::::

You can create a Shibboleth superuser using::

    python manage.py createsuperuser

Make sure the user login corresponds to a real user authenticated by Shibboleth.
When you are prompted for a password, insert a fake one.
`first_name`, `last_name`, `email` and `password` (to `'(not used)'`) will be
updated upon the first login.

You can also create or define an existing user as a superuser or staff in the
administration interface (if you have access and permission to do so).

:::::::::::::::::::::::::::::::::::::::
Configure authorizations for your views
:::::::::::::::::::::::::::::::::::::::

Because Shibboleth Authentication Backend is for the Django user authentication
system, please refer to `User authentication in Django`_ for more information.

.. _User authentication in Django: http://docs.djangoproject.com/en/dev/topics/auth/


:::::::::::::::::::::::::::::::::::::::                                                                                                                     
Notes                                                                                                                    
::::::::::::::::::::::::::::::::::::::: 
Every time a user logs in, only groups that exists both in the Group table and your CERN account are set. Any other group that you have setup programatically, will be erased. This means that you cannot assign a Django Group that does not come with your CERN account to anybody.

::::
FAQs
::::

* Why doesn't my administration interface use Shibboleth to log in users?

  On your `settings.py` make sure that you:

  * have added `shibsso` to `settings.INSTALLED_APPS` right after
    `django.contrib.admin` and before all your applications;

  * have set `settings.SHIB_SSO_ADMIN` to `True`.

* Some applications do not appear in the administration interface:

  On your `settings.py` make sure that you:

  * have added `shibsso` to `settings.INSTALLED_APPS` right after
    `django.contrib.admin` and before all your applications.

.. |R| unicode:: 0xAE .. Registered trademark symbol
