%define python_bin /usr/bin/python
%{!?python_sitelib: %define python_sitelib %(%{python_bin} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name: django-shibsso
Version: 0.2
Release: 2%{?dist}
Summary: Shibboleth (R) Authentication Backend for Django (R)
BuildArch: noarch

Group: Development/Libraries
License: Apache License, Version 2.0 
URL: https://git.cern.ch/web/itmon/django-shibsso.git
Source: %{name}.tar.gz
Packager: David Rodrigues
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

BuildRequires: python-devel
BuildRequires: python-setuptools
Requires: Django

%description
Shibboleth (R) Authentication Backend for Django (R).

%prep
%setup -n %{name}

%build
#CFLAGS="$RPM_OPT_FLAGS" 
%{python_bin} setup.py build

%install
rm -rf $RPM_BUILD_ROOT

%{python_bin} setup.py install --skip-build --root $RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root, 0755)
%doc LICENSE NOTICE README.rst
%{python_sitelib}/shibsso/
%{python_sitelib}/django_shibsso-%{version}-py*.egg-info/

%changelog
* Wed Jan 21 2015 Borja Garrido <borja.garrido.bear@cern.ch> - 0.2-2
- [AM-1357] Solved problem with admin url

* Wed Jan 14 2015 Borja Garrido <borja.garrido.bear@cern.ch> - 0.2-1
- [AM-408] Update Django version to 1.6
- Migrated the code from svn
